package file.load;

import java.io.*;
import java.util.ArrayList;

import delivery.Manifest;
import exceptions.CSVFormatException;
import exceptions.StockException;
import delivery.OrdinaryTruck;
import delivery.RefrigeratedTruck;
import delivery.Truck;
import exceptions.TemperatureException;
import exceptions.TruckException;
import stock.Item;
import stock.Stock;
import store.Store;

/**
 * This class contains various methods for parsing the various CSV files needed
 * 
 * @author Cain Kroker
 *
 */
public class LoadCSV {

	/**
	 * Loads the item properties into memory
	 * 
	 * @param fileToLoad
	 *            The file which contains the item properties
	 * @throws CSVFormatException
	 *             Throws this exception if the file format is wrong
	 * @throws IOException
	 *             Throws when loading the file if there is a problem
	 */
	public static void parseItemProperties(File fileToLoad) throws CSVFormatException, IOException {
		FileReader reader = new FileReader(fileToLoad);
		BufferedReader bReader = new BufferedReader(reader);
		while (bReader.ready()) {
			String[] t = bReader.readLine().split(",");
			try {
				if (t.length == 5) {
					Store.getInstance().getInventory().add(new Item(t[0], Double.parseDouble(t[1]), Double.parseDouble(t[2]),
							Integer.parseInt(t[3]), Integer.parseInt(t[4])));
				} else if (t.length == 6) {
					Store.getInstance().getInventory().add(new Item(t[0], Double.parseDouble(t[1]), Double.parseDouble(t[2]),
							Integer.parseInt(t[3]), Integer.parseInt(t[4]), Integer.parseInt(t[5])));
				} else {
					bReader.close();
					String exMessage = "Item Properties Load Failed:"
							+ System.lineSeparator()
							+ "Not a valid Item Properties file.";
					throw new CSVFormatException(exMessage);
				}
			} catch (NumberFormatException e) {
				bReader.close();
				String exMessage = "Item properties Load Failed:"
						+ System.lineSeparator()
						+ "Item Properties are invalid, name should be text, other properties should all be numbers.";
				throw new CSVFormatException(exMessage);
			}
		}
		bReader.close();
	}

	/**
	 * Loads a manifest and adds those item quantities to the store stock while
	 * decreasing the store's capital.
	 * 
	 * @param fileToLoad
	 *            The manifest to load
	 * @throws IOException
	 *             Thrown when the file fails to load
	 * @throws CSVFormatException
	 *             Thrown when an incorrectly loaded CSV file is used
	 * 
	 * @throws StockException
	 *             Thrown when there are Stock-related issues.
	 * @throws TemperatureException
	 *             Thrown when there are Temperature-related issues.
	 * @throws TruckException
	 *             Thrown when there are Truck-related issues (such as adding too
	 *             much cargo).
	 */
	public static void parseManifests(File fileToLoad)
			throws IOException, CSVFormatException, StockException, TemperatureException, TruckException {
		String manifestText = "";
		ArrayList<Truck> trucks = new ArrayList<Truck>();
		FileReader reader = new FileReader(fileToLoad);
		BufferedReader bReader = new BufferedReader(reader);

		// Read entire csv file to string so we can access more than one line at a time
		while (bReader.ready()) {
			manifestText += bReader.readLine();
			manifestText += System.lineSeparator();
		}
		bReader.close();
		
		// Check that the file is a valid Manifest, a truck should be the first thing in the file
		if (!(manifestText.startsWith(">"))) {
			String exMessage = "Manifest Load Failed: "
					+ System.lineSeparator()
					+ "Either file is not a Manifest, or Manifest is corrupted.";
			
			throw new CSVFormatException(exMessage);
		}

		// Split manifest into trucks
		String[] trucksText = manifestText.split(">");

		// Parse each truck
		// first element in manifest string is always empty stuff before first ">"
		for (int i = 1; i < trucksText.length; i++) {

			// Get Truck Items in Truck Stock
			Stock truckStock = new Stock();
			String[] truckStockText = trucksText[i].split(System.lineSeparator());

			for (int j = 1; j < truckStockText.length; j++) {
				String[] itemText = truckStockText[j].split(",");
				Item item = Store.getInstance().getInventory().find(itemText[0]);

				if (item == null) {
					String exMessage = "Manifest Load Failed: Unknown Item in Manifest." + System.lineSeparator()
							+ "Attempted Item to Load: " + itemText[0];
					throw new CSVFormatException(exMessage);
				}

				try {
					Integer quantity = Integer.parseInt(itemText[1]);
					truckStock.add(item, quantity);
				} catch (NumberFormatException e) {
					String exMessage = "Manifest Load Failed:"
							+ System.lineSeparator()
							+ "Manifest Data is invalid, name should be text, quantity should be a whole number.";
					throw new CSVFormatException(exMessage);
				}
			}

			// Check Truck Type to init proper truck and add stock to it
			// (with correct temperature for RefrigeratedTruck, based on lowest-temp item
			// inside RefrigeratedTruck)
			Integer truckTemp = 10;
			switch (truckStockText[0]) {
			case "Refrigerated":
				for (Item item : truckStock.getAllItems()) {
					if (item.getTemperature() != null && item.getTemperature() < truckTemp)
						truckTemp = item.getTemperature();
				}
				trucks.add(new RefrigeratedTruck(truckStock, truckTemp));
				break;

			case "Ordinary":
				trucks.add(new OrdinaryTruck(truckStock));
				break;

			default:
				String exMessage = "Manifest Load Failed: Invalid truck type in Manifest." + System.lineSeparator()
						+ "Attempted Truck Type to Load: " + truckStockText[0];

				throw new CSVFormatException(exMessage);
			}
		}

		// Add trucks to Manifest to calculate truck cost
		// Subtract Cost of items from Store Capital
		// Add newly delivered items to Store Inventory
		for (Truck t : trucks) {
			Manifest.add(t);
			Stock cargo = t.getCargo();
			for (Item item : cargo.getAllItems()) {
				Store.getInstance().addCapital(-(item.getManuCost() * cargo.getItemQuantity(item)));
				Store.getInstance().getInventory().add(item, cargo.getItemQuantity(item));
			}
		}

		// Subtract cost of all trucks in Manifest from Store Capital
		Store.getInstance().addCapital(-(Manifest.getTotalPrice()));
		Manifest.manifest.clear();
	}

	/**
	 * Loads a sales log and subtracts the item quantity from the item while adding
	 * to the store's capital
	 * 
	 * @param fileToLoad
	 *            The sales log to load
	 * @throws IOException
	 *             Thrown when the files fails to load
	 * @throws CSVFormatException
	 *             Thrown when an incorrect CSV format is used
	 * @throws StockException
	 *             Thrown when Stock cannot be subtracted from Inventory
	 */
	public static void parseSalesLog(File fileToLoad) throws IOException, CSVFormatException, StockException {
		FileReader reader = new FileReader(fileToLoad);
		BufferedReader bReader = new BufferedReader(reader);

		Stock stockToCheck = new Stock();

		while (bReader.ready()) {
			String[] t = bReader.readLine().split(",");
			
			if (t.length != 2) {
				bReader.close();
				String exMessage = "Sales Log Load Failed:"
						+ System.lineSeparator()
						+ "Not a valid Sales Log file.";
				throw new CSVFormatException(exMessage);
			}
			
			Item item = Store.getInstance().getInventory().find(t[0]);
			if (item == null) {
				bReader.close();
				String exMessage = "Sales Log Load Failed: Unknown Item in Sales Log."
						+ System.lineSeparator()
						+ "Attempted Item to Load: " + t[0];
				throw new CSVFormatException(exMessage);
			}
			try {
				stockToCheck.add(item, Integer.parseInt(t[1]));
			} catch (NumberFormatException e) {
				bReader.close();
				String exMessage = "Sales Log Load Failed:"
						+ System.lineSeparator()
						+ "Sales Log Data is invalid, name should be text, quantity should be a whole number.";
				throw new CSVFormatException(exMessage);
			}
			
		}
		bReader.close();

		// First loop to check to ensure Store has enough quantity of items trying to be
		// sold
		Stock storeStock = Store.getInstance().getInventory();
		for (Item item : stockToCheck.getAllItems()) {
			Integer storeQty = storeStock.getItemQuantity(storeStock.find(item.getName()));
			Integer salesQty = stockToCheck.getItemQuantity(item);

			if (storeQty < salesQty) {
				String exMessage = "Cannot load sales log: Insufficient Stock to fulfill Sales Log."
						+ System.lineSeparator() + "Item: " + item.getName() + System.lineSeparator()
						+ "Current Quantity in Stock: " + storeQty + System.lineSeparator()
						+ "Quantity attempted to sell: " + salesQty;

				throw new StockException(exMessage);
			}
		}

		// Second loop to actually process sales log after quantity check is complete
		for (Item item : stockToCheck.getAllItems()) {
			Integer qty = stockToCheck.getItemQuantity(item);
			Store.getInstance().getInventory().subtract(item.getName(), qty);
			System.out.println(fileToLoad.getName() + ":" + item.getPrice() * qty);
			Store.getInstance().addCapital(item.getPrice() * qty);
		}
	}
}
