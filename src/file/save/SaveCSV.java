package file.save;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

import delivery.Manifest;

import delivery.OrdinaryTruck;
import delivery.RefrigeratedTruck;
import exceptions.StockException;
import exceptions.TemperatureException;
import exceptions.TruckException;

import stock.Item;
import stock.Stock;
import store.Store;

/**
 * This class contains the methods needed to create and save manifest files
 * 
 * @author Cain Kroker
 *
 */
public class SaveCSV {

	/**
	 * Creates a manifest based on what is stored in the Manifest list.
	 * 
	 * @param fileName
	 *            The name of the manifest file
	 * @throws IOException
	 *             Throws when it can't save the file
	 * @throws TemperatureException
	 * 			Thrown when an expection with the temperature happens
	 * @throws StockException
	 * 			Thrown when an expection with the stock happens
	 * @throws TruckException
	 * 			Thrown when an expection with the truck happens
	 */
	public static void createManifestFile(String fileName)
			throws IOException, TemperatureException, StockException, TruckException {
		Stock storeStock = Store.getInstance().getInventory();
		Stock coldStock = new Stock();
		Stock ordinaryStock = new Stock();

		Integer qty = 0;
		Integer qty1 = 0;
		for (Item item : storeStock.getAllItems()) {
			if (storeStock.getItemQuantity(item) <= item.getReorderPoint()) {

				if (item.getTemperature() != null) {
					coldStock.add(item, item.getReorderAmount());
					qty1 += item.getReorderAmount();
				} else {
					ordinaryStock.add(item, item.getReorderAmount());
					qty += item.getReorderAmount();
				}
			}
		}

		ArrayList<Item> coldItems = new ArrayList<Item>();
		for (Item item : coldStock.getAllItems()) {
			coldItems.add(item);
		}

		coldItems.sort(new Comparator<Item>() {

			@Override
			public int compare(Item arg0, Item arg1) {
				if (arg0.getTemperature() < arg1.getTemperature())
					return -1;
				if (arg0.getTemperature() > arg1.getTemperature())
					return 1;
				return 0;
			}

		});
		Stock orderedStock = new Stock();
		for (Item i : coldItems) {
			orderedStock.add(i, i.getReorderAmount());
		}

		for (int i = 0; i <= (qty1 / 800); i++) {
			RefrigeratedTruck current = new RefrigeratedTruck(coldItems.get(0).getTemperature());
			Stock tempStock = current.splitStock(orderedStock);
			current.addCargo(tempStock);
			if (i == qty1 / 800 && current.getCurrentLoad() < current.getCapacity()) {
				tempStock = current.splitStock(ordinaryStock, current.getCurrentLoad());
				current.addCargo(tempStock);
			}
			for (Item a : current.getCargo().getAllItems()) {
				if (ordinaryStock.find(a.getName()) != null
						&& current.getCargo().getItemQuantity(a) >= ordinaryStock.getItemQuantity(a)) {
					ordinaryStock.remove(a);
				} else if (ordinaryStock.find(a.getName()) != null) {
					ordinaryStock.subtract(a.getName(), tempStock.getItemQuantity(a));
				}
				if (orderedStock.find(a.getName()) != null
						&& current.getCargo().getItemQuantity(a) >= orderedStock.getItemQuantity(a)) {
					orderedStock.remove(a);
					coldItems.remove(a);
				} else if (orderedStock.find(a.getName()) != null) {
					orderedStock.subtract(a.getName(), tempStock.getItemQuantity(a));
				}

			}
			if (current.getCurrentLoad() > 0)
				Manifest.add(current);
		}

		for (int i = 0; i <= (qty / 1000); i++) {
			OrdinaryTruck current = new OrdinaryTruck();
			Stock tempStock = current.splitStock(ordinaryStock);
			current.addCargo(tempStock);
			for (Item a : current.getCargo().getAllItems()) {
				if (current.getCargo().getItemQuantity(a) >= ordinaryStock.getItemQuantity(a)) {
					ordinaryStock.remove(a);
				} else {
					ordinaryStock.subtract(a.getName(), tempStock.getItemQuantity(a));
				}
			}
			if (current.getCurrentLoad() > 0)
				Manifest.add(current);

		}

		FileWriter fw = new FileWriter(fileName);
		fw.write(Manifest.toCsvString());
		Manifest.manifest.clear();
		fw.close();
	}

}
