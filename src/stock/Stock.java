package stock;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import exceptions.StockException;

/**
 * Class specifying the Stock object, a collection of Item objects, storing a
 * group of unique Items, and a quantity for each.
 * 
 * @author Russell McLeod
 *
 */

public class Stock {

	/**
	 * HashMap to store the Items and the Quantity for each item in the stock
	 */
	private LinkedHashMap<Item, Integer> stock;

	/**
	 * Main Constructor
	 */
	public Stock() {
		stock = new LinkedHashMap<Item, Integer>();
	}

	/**
	 * Method to Add an Item to the Stock collection
	 * 
	 * @param item
	 *            the Item in the Stock collection to Add/Update
	 */
	public void add(Item item) {
		if (stock.containsKey(item)) {
			stock.put(item, stock.get(item) + 1);
		} else {
			stock.putIfAbsent(item, new Integer(0));
		}
	}

	/**
	 * Method to add multiple of the same item into the stock
	 * 
	 * @param item
	 *            the Item in the Stock collection to Add/Update
	 * @param quantity
	 *            How much of the item to add
	 */
	public void add(Item item, Integer quantity) {
		if (stock.containsKey(item)) {
			stock.put(item, stock.get(item) + quantity);
		} else {
			stock.putIfAbsent(item, new Integer(quantity));
		}
	}

	/**
	 * Method to Find an Item inside the Stock collection.
	 * 
	 * @param name
	 *            String name of the item to find
	 * @return The Item object if it exists with the Stock collection, null
	 *         otherwise.
	 */
	public Item find(String name) {
		for (Item itm : stock.keySet()) {
			// if the Item is found in Stock
			if (itm.getName().equals(name)) {
				return itm;
			}
		}
		// if Item is not found
		return null;
	}

	/**
	 * Method to get the total Manufacture Cost (in dollars) of all Items inside the
	 * Stock collection.
	 * 
	 * @return The Manufacture Cost (in dollars) of the entire Stock collection.
	 */
	public double getManuCost() {
		double price = 0.0;
		for (Item itm : stock.keySet()) {
			price += (itm.getManuCost() * stock.get(itm));
		}
		return price;
	}

	/**
	 * Method to get the total Sell Price (in dollars) of all Items inside the Stock
	 * collection.
	 * 
	 * @return The Sell Price (in dollars) of the entire Stock collection.
	 */
	public double getPrice() {
		double price = 0.0;
		for (Item itm : stock.keySet()) {
			price += (itm.getPrice() * stock.get(itm));
		}
		return price;
	}

	/**
	 * Method to get Quantity of all items in Stock collection
	 * 
	 * @return Total quantity of all items in Stock Collection
	 */
	public int getQuantity() {
		int qty = 0;
		for (Item itm : stock.keySet()) {
			qty += stock.get(itm);
		}

		return qty;
	}

	/**
	 * Method to get all unique Items in the Stock collection.
	 * 
	 * @return A Set of all the Items in the Stock collection.
	 */
	public Set<Item> getAllItems() {
		return stock.keySet();
	}

	/**
	 * Method to get the quantity of a specific Item inside the Stock collection.
	 * 
	 * @param item
	 *            An Item inside the Stock collection.
	 * @return The Quantity of the specified Item within the Stock collection.
	 */
	public Integer getItemQuantity(Item item) {
		return stock.get(item);
	}

	/**
	 * Method to get CSV-Format String representation of a collection of Stock.
	 * 
	 * @return A CSV-Formatted String representation of the Stock collection.
	 */
	public String ToCsvString() {
		String value = "";

		for (HashMap.Entry<Item, Integer> entry : stock.entrySet()) {
			value += (entry.getKey().getName() + "," + entry.getValue());
			value += System.lineSeparator();
		}

		// dirty line to kill off last trailing newline
		value = value.substring(0, value.length() - System.lineSeparator().length());

		return value;
	}

	/**
	 * Subtracts some quantity of an Item from a Stock.
	 * 
	 * @param itemName
	 *            The Item to subtract.
	 * @param quantity
	 *            The quantity of the Item to subtract.
	 * @throws StockException
	 *             Thrown if a subtraction of a quantity higher than the available
	 *             amount is attempted.
	 */
	public void subtract(String itemName, int quantity) throws StockException {
		Item item = this.find(itemName);

		String exMessage = "";

		if (item == null) {
			exMessage = "Cannot subtract item: Item does not exist on Stock." + System.lineSeparator() + "Item: "
					+ itemName;
			throw new StockException(exMessage);
		}

		int qty = this.getItemQuantity(item);
		if (qty < quantity) {
			exMessage = "Cannot subtract item: Item in Stock has lower quantity than what is trying to be subtracted."
					+ System.lineSeparator() + "Item: " + item.getName() + System.lineSeparator() + "Current Quantity: "
					+ qty + System.lineSeparator() + "Quantity to remove: " + quantity;
			throw new StockException(exMessage);
		}

		stock.put(item, stock.get(item) - quantity);
	}

	/**
	 * Completely removes an Item from the Stock, based on the Item's name.
	 * 
	 * @param itemName
	 *            The string of the item name to remove
	 */
	public void remove(String itemName) {
		Item item = this.find(itemName);
		if (item != null) {
			stock.remove(item);
		}
	}

	/**
	 * Completely Removes a given Item from the Stock.
	 * 
	 * @param item
	 *            The item object to remove
	 */
	public void remove(Item item) {
		if (item != null) {
			stock.remove(item);
		}
	}
}
