package stock;

/**
 * Class specifying the Item object.
 * 
 * @author Russell McLeod
 *
 */

public class Item {

	/**
	 * The Name of the Item
	 */
	private String name;

	/**
	 * The Manufacturing Cost/Wholesale Price (in dollars) of the Item
	 */
	private double manuCost;

	/**
	 * The retail Selling Price (in dollars) of the Item
	 */
	private double sellPrice;

	/**
	 * The Reorder Point (minimum quantity of the item before reorder is required)
	 * of the Item
	 */
	private int reorderPoint;

	/**
	 * The Reorder Point (minimum quantity of the item before reorder is required)
	 * of the Item
	 */
	private int reorderAmount;

	/**
	 * The Temperature (in Celsius) at which the item needs to be stored.
	 */
	private Integer temperature = null;

	/**
	 * Main Constructor for item without specific storage temperature
	 * 
	 * @param name
	 *            The Name of the Item
	 * @param manuCost
	 *            The Manufacturing Cost/Wholesale Price (in dollars) of the Item
	 * @param sellPrice
	 *            The retail Selling Price (in dollars) of the Item
	 * @param reorderPoint
	 *            The Reorder Point (minimum quantity of the item before reorder is
	 *            required) of the Item
	 * @param reorderAmount
	 *            The Reorder Amount (quantity of the item that is ordered during
	 *            reorder) of the Item
	 */
	public Item(String name, double manuCost, double sellPrice, int reorderPoint, int reorderAmount) {
		this.name = name;
		this.manuCost = manuCost;
		this.sellPrice = sellPrice;
		this.reorderPoint = reorderPoint;
		this.reorderAmount = reorderAmount;
	}

	/**
	 * Constructor for item with specific storage temperature
	 * 
	 * @param name
	 *            The Name of the Item
	 * @param manuCost
	 *            The Manufacturing Cost/Wholesale Price (in dollars) of the Item
	 * @param sellPrice
	 *            The retail Selling Price (in dollars) of the Item
	 * @param reorderPoint
	 *            The Reorder Point (minimum quantity of the item before reorder is
	 *            required) of the Item
	 * @param reorderAmount
	 *            The Reorder Amount (quantity of the item that is ordered during
	 *            reorder) of the Item
	 * @param temperature
	 *            The Temperature (in Celsius) at which the item needs to be stored.
	 */
	public Item(String name, double manuCost, double sellPrice, int reorderPoint, int reorderAmount,
			Integer temperature) {
		this(name, manuCost, sellPrice, reorderPoint, reorderAmount);
		this.temperature = temperature;
	}

	/**
	 * Getter method for Item Name
	 * 
	 * @return The Name of the Item
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter method for Manufacturing Cost
	 * 
	 * @return The Manufacturing Cost of the Item
	 */
	public double getManuCost() {
		return manuCost;
	}

	/**
	 * Getter method for Sell Price
	 * 
	 * @return The retail Sell Price of the Item
	 */
	public double getPrice() {
		return sellPrice;
	}

	/**
	 * Getter method for Reorder Point
	 * 
	 * @return The Reorder Point of the Item
	 */
	public int getReorderPoint() {
		return reorderPoint;
	}

	/**
	 * Getter method for Reorder Amount
	 * 
	 * @return The Reorder Amount for the Item
	 */
	public int getReorderAmount() {
		return reorderAmount;
	}

	/**
	 * Getter method to obtain string representation of storage Temperature.
	 * 
	 * @return The String representation of the storage Temperature of the Item,
	 *         regardless of whether or not it has a specific storage Temperature.
	 */
	public Integer getTemperature() {
		return temperature;
	}

	/**
	 * Setter method for Item Name
	 * 
	 * @param name
	 *            The new Name of the Item
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Setter Method for Manufacturing Cost
	 * 
	 * @param manuCost
	 *            The new Manufacturing Cost (in dollars) for the Item
	 */
	public void setManuCost(double manuCost) {
		this.manuCost = manuCost;
	}

	/**
	 * Setter method for Sell Price
	 * 
	 * @param sellPrice
	 *            The new retail Sell Price (in dollars) for the Item
	 */
	public void setPrice(double sellPrice) {
		this.sellPrice = sellPrice;
	}

	/**
	 * Setter method for Reorder Point
	 * 
	 * @param reorderPoint
	 *            The new Reorder Point (minimum quantity of the item before reorder
	 *            is required) for the Item
	 */
	public void setReorderPoint(int reorderPoint) {
		this.reorderPoint = reorderPoint;
	}

	/**
	 * Setter method for Reorder Amount
	 * 
	 * @param reorderAmount
	 *            The new Reorder Amount (quantity of the item that is ordered
	 *            during reorder) for the Item
	 */
	public void setReorderAmount(int reorderAmount) {
		this.reorderAmount = reorderAmount;
	}

	/**
	 * Setter method for Temperature
	 * 
	 * @param temperature
	 *            The new keeping Temperature for this Item
	 */
	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}
}
