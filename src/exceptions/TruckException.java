package exceptions;

/**
 * Wrapper for Exception class, used for handling Truck-related issues.
 */
public class TruckException extends Exception {

	/**
	 * Main Constructor
	 * 
	 * @param message
	 *            Message to add to the exception.
	 */
	public TruckException(String message) {
		super(message);
	}
}
