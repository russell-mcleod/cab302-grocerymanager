package exceptions;

/**
 * Wrapper for Exception class, used for handling CSV-format-related issues.
 */
public class CSVFormatException extends Exception {

	/**
	 * Main Constructor
	 * 
	 * @param message
	 *            Message to add to the exception.
	 */
	public CSVFormatException(String message) {
		super(message);
	}
}
