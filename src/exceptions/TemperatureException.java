package exceptions;

/**
 * Wrapper for Exception class, used for handling Temperature-related issues.
 */
public class TemperatureException extends Exception {

	/**
	 * Main Constructor
	 * 
	 * @param message
	 *            Message to add to the exception.
	 */
	public TemperatureException(String message) {
		super(message);
	}
}
