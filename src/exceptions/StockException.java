package exceptions;

/**
 * Wrapper for Exception class, used for handling Stock-related issues.
 * 
 * @author Russell McLeod
 *
 */
public class StockException extends Exception {

	/**
	 * Main Constructor
	 * 
	 * @param message
	 *            Message to add to the exception.
	 */
	public StockException(String message) {
		super(message);
	}
}
