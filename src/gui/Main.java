package gui;

import java.awt.Dimension;

/**
 * Program entry point
 * 
 * @author Cain
 *
 */
public class Main {

	public static void main(String[] args) {
		GUI gui = new GUI("SuperMart Store Inventory Manager", new Dimension(800, 600));
	}
}
