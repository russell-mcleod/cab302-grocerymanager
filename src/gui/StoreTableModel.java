package gui;

import javax.swing.table.AbstractTableModel;

import stock.Item;
import store.Store;

/**
 * The TableModel for our JTable
 * 
 * @author Cain Kroker
 *
 */
public class StoreTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private String[] columnNames = { "Name", "Quantity", "Manu. Cost ($)", "Sell Price ($)", "Reorder Point",
			"Reorder Amount", "Temperature (�C)" };
	private Object[] allItems = Store.getInstance().getInventory().getAllItems().toArray();
	private Object[][] data = new Object[allItems.length][7];

	public StoreTableModel() {
	}

	/**
	 * Gets the name of the column
	 */
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/**
	 * Used to load the data into the table. Can also be called to reload the data
	 * in the table.
	 */
	public void loadData() {
		allItems = Store.getInstance().getInventory().getAllItems().toArray();
		data = new Object[allItems.length][7];
		for (int i = 0; i < allItems.length; i++) {
			data[i][0] = ((Item) allItems[i]).getName();
			data[i][1] = Store.getInstance().getInventory().getItemQuantity((Item) allItems[i]);
			data[i][2] = ((Item) allItems[i]).getManuCost();
			data[i][3] = ((Item) allItems[i]).getPrice();
			data[i][4] = ((Item) allItems[i]).getReorderPoint();
			data[i][5] = ((Item) allItems[i]).getReorderAmount();
			data[i][6] = ((Item) allItems[i]).getTemperature();
		}
		this.fireTableDataChanged();
	}

	/**
	 * Gets the amount of columns in the table.
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/**
	 * Gets the amount of rows in the table.
	 */
	@Override
	public int getRowCount() {
		return data.length;
	}

	/**
	 * Gets the value of the cell at the given row and column coordinates.
	 * 
	 * @param row
	 *            The row coordinate of the cell.
	 * @param column
	 *            The column coordinate of the cell.
	 */
	@Override
	public Object getValueAt(int row, int column) {
		return data[row][column];
	}
}
