package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import exceptions.CSVFormatException;
import exceptions.StockException;
import exceptions.TemperatureException;
import exceptions.TruckException;

import file.load.LoadCSV;
import file.save.SaveCSV;
import store.Store;

/**
 * The GUI that displays all the data about the store.
 * 
 * @author Cain Kroker
 * @author Russell McLeod
 *
 */
public class GUI extends JFrame {

	private static final long serialVersionUID = 6372807516903639954L;

	/**
	 * The panel holding the Store name and Capital labels.
	 */
	private JPanel pnlStore;
	/**
	 * The panel holding the Buttons.
	 */
	private JPanel pnlButtons;

	// The 4 buttons in the GUI
	private JButton btnLoadItems;
	private JButton btnExportManifest;
	private JButton btnLoadManifest;
	private JButton btnLoadSalesLog;

	// The labels for Store Name and Capital
	private JLabel lblCapital;
	private JLabel lblName;

	/**
	 * The GUI's main menu bar.
	 */
	private JMenuBar menuBar = new JMenuBar();

	/**
	 * The main panel for the inventory.
	 */
	private JPanel pnlInventory;
	private JScrollPane jspInventory;

	/**
	 * The Table displaying the Store inventory.
	 */
	private JTable tblInventory;
	private StoreTableModel storeModel;

	/**
	 * boolean to hold whether or not the Item Properties have been loaded into the
	 * program.
	 */
	private boolean hasItemPropsLoaded = false;

	/**
	 * The single instance of the Store itself.
	 */
	Store store = Store.getInstance();

	/**
	 * The constructor to create a GUI
	 * 
	 * @param title
	 *            The title of the window
	 * @param size
	 *            The size of the window
	 */
	public GUI(String title, Dimension size) {
		super(title);
		JFrame.setDefaultLookAndFeelDecorated(true);
		this.setSize(size);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setEnabled(true);

		createMenuBar();
		this.setJMenuBar(menuBar);
		initUI();

		this.setVisible(true);
	}

	/**
	 * Initializes the interface
	 */
	private void initUI() {
		lblName = createJLabel("Store Name: " + Store.getInstance().getName());
		lblCapital = createJLabel("Available Capital: " + Store.getInstance().getFormattedCapital());

		initStoreData();
		initButtonPanel();

		inventoryLayout();
		pnlInventory = createJPanel(new BorderLayout());

		jspInventory = new JScrollPane(tblInventory);

		pnlInventory.add(pnlButtons, BorderLayout.NORTH);
		pnlInventory.add(jspInventory, BorderLayout.CENTER);

		pnlInventory.setBorder(BorderFactory.createEtchedBorder());

		this.add(pnlStore, BorderLayout.NORTH);
		this.add(pnlInventory, BorderLayout.CENTER);
	}

	/**
	 * Creates the menu bar and adds the items with actions
	 */
	private void createMenuBar() {
		JMenu itemMenu = new JMenu("Item");
		itemMenu.add(new JMenuItem("Load Item Properties")).addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				LoadItemProperties();
			}
		});
		;
		menuBar.add(itemMenu);

		JMenu manifestMenu = new JMenu("Manifest");
		manifestMenu.add(new JMenuItem("Export Manifest")).addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ExportManifest();
			}
		});
		;

		manifestMenu.add(new JMenuItem("Load Manifest")).addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				LoadManifest();
			}
		});
		;
		menuBar.add(manifestMenu);

		JMenu salesMenu = new JMenu("Sales");
		salesMenu.add(new JMenuItem("Load Sales Log")).addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				LoadSalesLog();
			}
		});
		;
		menuBar.add(salesMenu);
	}

	/**
	 * Sets up the layout for the main store panel and adds need components
	 */
	private void initStoreData() {
		pnlStore = createJPanel(new BorderLayout());
		pnlStore.add(lblName, BorderLayout.WEST);
		pnlStore.add(lblCapital, BorderLayout.EAST);
	}

	/**
	 * Sets up the layout for the inventory display.
	 */
	private void inventoryLayout() {
		storeModel = new StoreTableModel();
		tblInventory = new JTable(storeModel);
		tblInventory.getTableHeader().setReorderingAllowed(false);
		jspInventory = new JScrollPane();
		tblInventory.setFillsViewportHeight(true);
	}

	/**
	 * Sets up the a JPanel with a given LayoutManager
	 * 
	 * @return the panel
	 */
	private JPanel createJPanel(LayoutManager layout) {
		JPanel pnl = new JPanel();
		pnl.setLayout(layout);
		return pnl;
	}

	/**
	 * Creates a JLabel with the specified text
	 * 
	 * @param text
	 *            The specified text
	 * @return The created JLabel
	 */
	private JLabel createJLabel(String text) {
		JLabel lbl = new JLabel(text);
		return lbl;
	}

	/**
	 * Sets up the 4 main GUI buttons.
	 */
	private void initButtonPanel() {
		pnlButtons = createJPanel(new GridLayout(1, 4));

		btnLoadItems = new JButton("Load Item Properties");
		btnLoadItems.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				LoadItemProperties();
			}

		});
		pnlButtons.add(btnLoadItems);

		btnExportManifest = new JButton("Export Manifest");
		btnExportManifest.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ExportManifest();
			}

		});

		pnlButtons.add(btnExportManifest);

		btnLoadManifest = new JButton("Load Manifest");
		btnLoadManifest.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				LoadManifest();
			}

		});

		pnlButtons.add(btnLoadManifest);

		btnLoadSalesLog = new JButton("Load Sales Log");
		btnLoadSalesLog.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				LoadSalesLog();
			}

		});

		pnlButtons.add(btnLoadSalesLog);
		toggleButtons();
	}

	/**
	 * Enables or disables certain button controls depending on whether Item
	 * properties have been loaded.
	 */
	private void toggleButtons() {
		btnExportManifest.setEnabled(hasItemPropsLoaded);
		btnLoadManifest.setEnabled(hasItemPropsLoaded);
		btnLoadSalesLog.setEnabled(hasItemPropsLoaded);
		menuBar.getMenu(1).setEnabled(hasItemPropsLoaded);
		menuBar.getMenu(2).setEnabled(hasItemPropsLoaded);
		
		btnLoadItems.setEnabled(!hasItemPropsLoaded);
		menuBar.getMenu(0).setEnabled(!hasItemPropsLoaded);
	}

	/**
	 * Gets a CSV File from a FileChooser dialog.
	 * 
	 * @param title
	 *            The title of the FileChooser dialog.
	 * @return The selected CSV-Formatted file.
	 */
	private File GetCSVFromDialog(String title) {
		final JFileChooser fc = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV Formatted Text File (.csv)",
				new String[] { "csv" });
		fc.setFileFilter(filter);
		fc.setCurrentDirectory(new File("."));
		fc.setDialogTitle(title);

		int returnVal = fc.showOpenDialog(GUI.this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile();
		} else {
			return null;
		}
	}

	/**
	 * Loads the Item Properties into the program.
	 */
	private void LoadItemProperties() {
		File file = GetCSVFromDialog("Load Item Properties...");
		if (file != null) {
			try {
				LoadCSV.parseItemProperties(file);
				hasItemPropsLoaded = true;
				((StoreTableModel) tblInventory.getModel()).loadData();
				toggleButtons();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error loading Item Properties.",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Exports a Manifest based on data in inventory.
	 */
	private void ExportManifest() {
		try {
			File dir = new File("manifests/");
			dir.mkdir();

			String filePath = dir.getPath() + "/manifest_" + printCurrentDateTime() + ".csv";
			SaveCSV.createManifestFile(filePath);

			String message = "Manifest has been successfully exported to: " + System.lineSeparator() + filePath;

			JOptionPane.showMessageDialog(new JFrame(), message, "Manifest export successful.",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error exporting Manifest.",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Loads a Manifest and applies it to the Store's inventory.
	 */
	private void LoadManifest() {
		File file = GetCSVFromDialog("Load Manifest...");
		if (file != null) {
			try {
				LoadCSV.parseManifests(file);
				UpdateDataDisplay();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error loading Manifest.",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Loads a Sales Log and applies it to the Store's Inventory.
	 */
	private void LoadSalesLog() {
		File file = GetCSVFromDialog("Load Sales Log...");
		if (file != null) {
			try {
				LoadCSV.parseSalesLog(file);
				UpdateDataDisplay();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error loading Sales Log.",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Updates the Table display data and the Available Capital label.
	 */
	private void UpdateDataDisplay() {
		((StoreTableModel) tblInventory.getModel()).loadData();
		lblCapital.setText("Available Capital: " + Store.getInstance().getFormattedCapital());
	}

	/**
	 * Gets the current Date/Time in a readable format.
	 * 
	 * @return A readable String representation of the current Date/Time.
	 */
	private String printCurrentDateTime() {
		return new SimpleDateFormat("yyyy-MM-dd__kk.mm.ss").format(new Date());
	}
}
