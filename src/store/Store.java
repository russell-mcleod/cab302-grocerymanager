package store;

import stock.Stock;

/**
 * Class specifying the Store object. Implements Singleton pattern.
 * 
 * @author Russell McLeod
 *
 */
public class Store {

	/**
	 * Store's name
	 */
	private String name;

	/**
	 * Store's available capital (in dollars)
	 */
	private double capital;

	/**
	 * Store's current inventory of Stock
	 */
	private Stock inventory;

	/**
	 * Constructor for Store. Sets default name, starting capital, and new empty
	 * inventory of Stock. Should not be called, except by below StoreHolder Class
	 */
	protected Store() {
		this.name = "SuperMart";
		this.capital = 100000;
		this.inventory = new Stock();
	}

	/**
	 * Class to hold a single instance of Store. Creates a single instance of Store
	 * upon construction.
	 * 
	 * @author Russell McLeod
	 *
	 */
	private static class StoreHolder {
		private final static Store INSTANCE = new Store();
	}

	/**
	 * Getter method for Store. Gets the single Store instance.
	 * 
	 * @return the single instance of Store.
	 */
	public static Store getInstance() {
		return StoreHolder.INSTANCE;
	}

	/**
	 * Getter method for Store name.
	 * 
	 * @return The Store's Name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter method for Store Inventory
	 * 
	 * @return The Stock object containing the Store's Inventory.
	 */
	public Stock getInventory() {
		return inventory;
	}

	/**
	 * Getter method for Store Capital
	 * 
	 * @return The Store's current amount of Capital (in dollars).
	 */
	public double getCapital() {
		return capital;
	}

	/**
	 * Wrapper method to return Store Capital as a specifically-formatted String.
	 * 
	 * @return Currency-styled String representation of the Store Capital.
	 */
	public String getFormattedCapital() {
		return String.format("$%,.2f", getCapital());
	}

	/**
	 * Setter method for Store Capital May not be needed.
	 * 
	 * @param capital
	 *            The new Store Capital
	 */
	public void setCapital(double capital) {
		this.capital = capital;
	}

	/**
	 * Method to add (or subtract) capital to the Store Capital.
	 * 
	 * @param difference
	 *            The amount of capital to add (or negative number to subtract).
	 */
	public void addCapital(double difference) {
		this.capital += difference;
	}

}
