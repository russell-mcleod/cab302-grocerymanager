package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import delivery.RefrigeratedTruck;
import delivery.Truck;
import exceptions.StockException;
import exceptions.TemperatureException;
import exceptions.TruckException;
import stock.Item;
import stock.Stock;

/**
 * JUnit Test Class to test RefrigeratedTruck class
 * 
 * @author Russell McLeod
 *
 */
public class TestRefrigeratedTruck {

	RefrigeratedTruck testTruck1;
	Truck testTruck2;

	/*
	 * Test 0: Constructor Test with Truck Temperature
	 * 
	 */
	@Before
	@Test
	public void testInitTruck() throws TemperatureException {
		testTruck1 = new RefrigeratedTruck(3);
	}

	/**
	 * Test 1: Testing getter for Truck Temperature
	 */
	@Test
	public void testGetTemperature() {
		assertEquals("Failed to get correct temperature of Refrigerated Truck.", 3, testTruck1.getTemperature());
	}

	/**
	 * A Rule used by later tests to check Exception Types and their Messages
	 */
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	/**
	 * Test 2: Testing creating a Truck with temperature colder than allowed minimum
	 * 
	 * @throws TemperatureException
	 *             A TemperatureException due to initialization temperature being
	 *             too cold
	 */
	@Test
	public void testCreateTruckTooCold() throws TemperatureException {
		Integer temp = -21;

		thrown.expect(TemperatureException.class);
		String message = "Temperature is outside the allowed range!" + System.lineSeparator()
				+ "Attempted temperature: " + temp.toString() + System.lineSeparator()
				+ "Allowed Range: -20 Celsius to 10 Celsius.";
		thrown.expectMessage(message);

		testTruck2 = new RefrigeratedTruck(temp);
		fail("Expected a TemperatureException to be thrown; temperature is below allowed minimum.");
	}

	/**
	 * Test 3: Testing creating a Truck with temperature hotter than allowed maximum
	 * 
	 * @throws TemperatureException
	 *             A TemperatureException due to initialization temperature being
	 *             too hot
	 */
	@Test
	public void testCreateTruckTooHot() throws TemperatureException {
		Integer temp = 11;

		thrown.expect(TemperatureException.class);
		String message = "Temperature is outside the allowed range!" + System.lineSeparator()
				+ "Attempted temperature: " + temp.toString() + System.lineSeparator()
				+ "Allowed Range: -20 Celsius to 10 Celsius.";
		thrown.expectMessage(message);

		testTruck2 = new RefrigeratedTruck(temp);
		fail("Expected a TemperatureException to be thrown; temperature is above allowed maximum.");
	}

	/**
	 * Test 4: Testing getter for truck's capacity (should be constant)
	 */
	@Test
	public void testTruckCapacity() {
		assertEquals("Failed to get correct constant capacity for RefrigeratedTruck.", 800, testTruck1.getCapacity());
	}

	/**
	 * Test 5: Testing getting the truck's cargo
	 * 
	 * @throws TemperatureException
	 *             TemperatureException due to a Stock Item needing a temperature
	 *             lower than the truck's temperature.
	 * @throws TruckException
	 *             TruckException due to adding too much cargo too a Truck.
	 */
	@Test
	public void testAddGetCargo() throws TemperatureException, TruckException {
		Stock cargo = new Stock();

		cargo.add(new Item("Milk", 2.0, 3.0, 200, 300, 3));
		cargo.add(new Item("Bread", 1.0, 1.5, 100, 200));
		testTruck1.addCargo(cargo);

		assertEquals("Failed to get correct cargo Stock from Truck.", cargo.getAllItems(),
				testTruck1.getCargo().getAllItems());
	}

	/**
	 * Test 6: Testing adding cargo to a Truck where a cargo Item needs to be stored
	 * in a lower temperature than what the truck supports.
	 * 
	 * @throws TemperatureException
	 *             TemperatureException due to a Stock Item needing a temperature
	 *             lower than the truck's temperature.
	 * @throws TruckException
	 *             TruckException due to adding too much cargo too a Truck.
	 */
	@Test // (expected = TemperatureException.class)
	public void testAddTooColdCargo() throws TemperatureException, TruckException {
		Integer temp = 3;

		thrown.expect(TemperatureException.class);
		String message = "Temperature of a Cargo Item needs to be lower than this Truck supports!"
				+ System.lineSeparator() + "Attempted Item: " + "Liquid Nitrogen" + System.lineSeparator()
				+ "Item Temperature: " + -200 + System.lineSeparator() + "Allowed Range: " + temp.toString()
				+ " Celsius and higher.";
		thrown.expectMessage(message);

		Truck testTruck2 = new RefrigeratedTruck(temp);
		Stock cargo = new Stock();

		cargo.add(new Item("Milk", 2.0, 3.0, 200, 300, 3));
		cargo.add(new Item("Liquid Nitrogen", 250.0, 330.0, 0, 10, -200));
		testTruck2.addCargo(cargo);

		fail("Expected a TemperatureException to be thrown; cargo storage temperature is below Truck temperature.");
	}

	/**
	 * Test 7: Testing getting the total cost of the Truck.
	 */
	@Test
	public void testGetCost() {
		// this should calculate 900 + 200 x (0.7^(T/5))
		double calculatedCost = 900.0 + (200.0 * Math.pow(0.7, (testTruck1.getTemperature() / 5.0)));
		assertEquals("Failed to get correct Truck Cost.", calculatedCost, testTruck1.getCost(), 0);
	}

}
