package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import stock.Item;
import stock.Stock;
import store.Store;

/**
 * 
 * @author Cain Kroker
 *
 */
public class TestStore {

	/*
	 * Test 0 Creating two instances of Store
	 */
	Store store1;
	Store store2;

	/*
	 * Test 1: Initialising both instances of Store
	 */
	@Before
	@Test
	public void testInit() {
		store1 = Store.getInstance();
		store2 = Store.getInstance();
	}

	/*
	 * Test 2: Equality test
	 */
	@Test
	public void testEquality() {
		assertTrue(store1 == store2);
	}

	/*
	 * Test 3: Testing the getter for the store capital
	 */
	@Test
	public void testGetCapital() {
		assertEquals("Didn't get the correct capital", 100000, store1.getCapital(), 0);
	}

	/*
	 * Test 4: Testing the getter for the store inventory
	 */
	@Test
	public void testGetInventory() {
		/* Not sure what to compare this too */
		store1.getInventory();
	}

	/*
	 * Test 5: Testing the ability to add items to the inventory
	 */
	@Test
	public void testAddToInventory() {
		Stock testStock = new Stock();
		Item item = new Item("Bread", 0, 0, 0, 0);
		testStock.add(item);
		store1.getInventory().add(item);
		assertEquals("Inventories not the same", testStock.getAllItems(), store1.getInventory().getAllItems());
	}

	/*
	 * Test 6: Testing the getter for the store's name
	 */
	@Test
	public void testGetName() {
		assertEquals("Didn't get the correct name", "SuperMart", store1.getName());
	}

	/*
	 * Test 7: Testing the setting of the capital
	 */
	public void testSetCapital() {
		store1.addCapital(-100);
		assertEquals("Didn't change the capital to the correct value", 99900.0, store1.getCapital(), 0);
	}
}
