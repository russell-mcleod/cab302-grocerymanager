package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import stock.Item;

/**
 * 
 * @author Cain Kroker
 *
 */
public class TestItem {

	/*
	 * Test 0 Testing that the item class exists and is imported
	 */

	Item testItem1;
	Item testItem2;

	/*
	 * Test 1 Testing initialization with temperature and without a temperature.
	 */
	@Test
	@Before
	public void testInit1() {
		testItem1 = new Item("Milk", 10.0, 15.0, 100, 100, -10);
		testItem2 = new Item("Bread", 10.0, 15.0, 100, 100);
	}

	/*
	 * Test 2 Testing a getter for the name
	 */
	@Test
	public void testGetName() {
		assertEquals("Failed to get correct name", "Milk", testItem1.getName());
	}

	/*
	 * Test 3 Testing a getter for the manufacturing cost
	 */
	@Test
	public void testGetManuCost() {
		assertEquals("Failed to get correct manufacturing cost", 10.0, testItem1.getManuCost(), 0);
	}

	/*
	 * Test 4 Testing a getter for the selling price
	 */
	@Test
	public void testGetPrice() {
		assertEquals("Failed to get correct selling price", 15.0, testItem2.getPrice(), 0);
	}

	/*
	 * Test 5 Testing a getter for the reorder point
	 */
	@Test
	public void testGetReorderPoint() {
		assertEquals("Failed to get correct reorder point", 100, testItem1.getReorderPoint());
	}

	/*
	 * Test 6 Testing a getter for the reorder amount
	 */
	@Test
	public void testGetReorderAmount() {
		assertEquals("Failed to get correct reorder amount", 100, testItem1.getReorderAmount());
	}

	/*
	 * Test 7 Testing a getter for the temperature
	 */
	@Test
	public void testGetTemperature1() {
		assertEquals("Failed to get correct temperature", new Integer(-10), testItem1.getTemperature());
	}

	/*
	 * Test 8 Testing a getter for the temperature for an Item that was initialized
	 * without a temperature
	 */
	@Test
	public void testGetTemperature2() {
		assertEquals("Failed to get correct temperature", null, testItem2.getTemperature());
	}

	/*
	 * Test 9 Testing a setter for the name
	 */
	@Test
	public void testSetName() {
		testItem1.setName("Milk2");
		assertEquals("Failed to get correct name", "Milk2", testItem1.getName());
	}

	/*
	 * Test 10 Testing a setter for the manufacturing cost
	 */
	@Test
	public void testSetManuCost() {
		testItem1.setManuCost(10.5);
		assertEquals("Failed to get correct manufacturing cost", 10.5, testItem1.getManuCost(), 0);
	}

	/*
	 * Test 11 Testing a setter for the selling price
	 */
	@Test
	public void testSetPrice() {
		testItem2.setPrice(15.5);
		assertEquals("Failed to get correct selling price", 15.5, testItem2.getPrice(), 0);
	}

	/*
	 * Test 12 Testing a setter for the reorder point
	 */
	@Test
	public void testSetReorderPoint() {
		testItem1.setReorderPoint(150);
		assertEquals("Failed to get correct reorder point", 150, testItem1.getReorderPoint());
	}

	/*
	 * Test 13 Testing a setter for the reorder amount
	 */
	@Test
	public void testSetReorderAmount() {
		testItem1.setReorderAmount(200);
		assertEquals("Failed to get correct reorder amount", 200, testItem1.getReorderAmount());
	}

	/*
	 * Test 14 Testing a setter for the temperature
	 */
	@Test
	public void testSetTemperature1() {
		testItem2.setTemperature(-20);
		assertEquals("Failed to get correct temperature", new Integer(-20), testItem2.getTemperature());
	}

	/*
	 * Test 15 Testing a setter for the temperature for an Item that was initialized
	 * without a temperature
	 */
	@Test
	public void testSetTemperature2() {
		testItem1.setTemperature(-15);
		assertEquals("Failed to get correct temperature", new Integer(-15), testItem1.getTemperature());
	}
}
