package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import delivery.Manifest;
import delivery.OrdinaryTruck;
import delivery.RefrigeratedTruck;
import delivery.Truck;
import exceptions.StockException;
import exceptions.TemperatureException;
import exceptions.TruckException;
import stock.Item;
import stock.Stock;

/**
 * JUnit 4 Test Class for testing Manifest class
 * 
 * @author Russell McLeod
 *
 */
public class TestManifest {

	/*
	 * Test 1: Test Adding Trucks to Manifest and Getting Trucks from Manifest
	 * 
	 * @throws StockException
	 */
	@Test
	public void testAddGetTruck() throws StockException {
		Stock drygoods = new Stock();
		drygoods.add(new Item("Bread", 1.0, 1.5, 150, 200));
		drygoods.add(new Item("candybar", 0.05, 0.5, 300, 500));

		Stock coldgoods = new Stock();
		coldgoods.add((new Item("Milk", 2.0, 3.0, 200, 300, 3)));
		coldgoods.add((new Item("Ice Cream", 5.0, 7.0, 100, 200, -19)));

		try {
			Truck truck1 = new OrdinaryTruck(drygoods);
			Truck truck2 = new RefrigeratedTruck(coldgoods, -20);

			Manifest.add(truck1);
			Manifest.add(truck2);

			assertEquals("Failed to add/get first Truck within Manifest.", truck1, Manifest.get(3));
			assertEquals("Failed to add/get second Truck within Manifest.", truck2, Manifest.get(4));

		} catch (TruckException | TemperatureException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Test 2: Test removing a Truck from the Manifest based on index in Manifest
	 */
	@Test
	public void testRemoveFirstTruck() {
		Truck truck3 = new OrdinaryTruck();

		Manifest.add(truck3);
		Manifest.remove(truck3);

		assertFalse("Failed to remove first Truck in Manifest", Manifest.contains(truck3));
	}

	/*
	 * Test 3: Test removing a specific Truck from Manifest
	 */
	@Test
	public void testRemoveSpecificTruck() {
		try {
			Truck truck4 = new OrdinaryTruck();
			Truck truck5 = new RefrigeratedTruck(3);

			Manifest.add(truck4);
			Manifest.add(truck5);
			Manifest.remove(truck4);

			assertFalse("Failed to remove truck4 from Manifest", Manifest.contains(truck4));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Test 4: Test generating a CSV-Formatted String representation of a Manifest
	 * 
	 */
	@Test
	public void testManifestToCsvString() throws StockException {
		Item bread = new Item("Bread", 1.0, 1.5, 150, 200);
		Item candybar = new Item("candybar", 0.05, 0.5, 300, 500);
		Item milk = new Item("Milk", 2.0, 3.0, 200, 300, 3);
		Item icecream = new Item("Ice Cream", 5.0, 7.0, 100, 200, -19);

		Stock drygoods = new Stock();
		drygoods.add(bread);
		drygoods.add(candybar);
		drygoods.add(candybar);

		Stock coldgoods = new Stock();
		coldgoods.add(milk);
		coldgoods.add(icecream);

		System.out.println(coldgoods.ToCsvString());

		try {
			Truck truck6 = new OrdinaryTruck(drygoods);
			Truck truck7 = new RefrigeratedTruck(coldgoods, -19);

			Manifest.add(truck6);
			Manifest.add(truck7);

			String output = ">Ordinary" + System.lineSeparator() + truck6.getCargo().ToCsvString()
					+ System.lineSeparator() + ">Refrigerated" + System.lineSeparator()
					+ truck7.getCargo().ToCsvString() + System.lineSeparator();

			assertEquals("Faiiled to get correct CSV representation of Manifest.", output, Manifest.toCsvString());
		} catch (TruckException | TemperatureException e) {
			e.printStackTrace();
		}
	}
}
