package tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import exceptions.StockException;
import stock.Item;
import stock.Stock;

/**
 * 
 * @author Cain Kroker
 *
 */
public class TestStock {

	/*
	 * Test 0 Check the stock class exists and is imported correctly
	 */
	Stock storeInventory = new Stock();
	Stock truckCargo = new Stock();
	Item[] test = new Item[15];

	/*
	 * Test 1: Filling the stock with various items the items.
	 */
	@Before
	@Test
	public void testFillingStock() {
		for (int i = 0; i < 15; i++) {
			test[i] = new Item("test" + i, 10.0, 15.0, 100, 100, -10);
			storeInventory.add(test[i]);
		}
	}

	/*
	 * Test 2: Testing if the Store actually contains said items
	 */
	@Test
	public void testStore() {
		assertEquals("Didn't find the correct item or it doesn't exist", "test5",
				storeInventory.find("test5").getName());
	}

	/*
	 * Test 3: Gets the price of the whole selected inventory
	 */
	@Test
	public void testPrice() {
		assertEquals("Incorrect price", 0, storeInventory.getPrice(), 0);
	}

	/*
	 * Test 4: Gets all the items in a stock object
	 */
	@Test
	public void testGetAllItems() {
		Set<Item> items = new HashSet<Item>();
		items.addAll(Arrays.asList(test));
		assertEquals("Arrays don't match", items, storeInventory.getAllItems());
	}

	/*
	 * Test 5: Tests if it gets Item quantities
	 */
	@Test
	public void testGetItemQuantity() {
		assertEquals("Incorrect quantity", new Integer(0), storeInventory.getItemQuantity(test[1]));
	}

	/*
	 * Test 6: Test to remove an item
	 */
	@Test
	public void testRemoveItem() {
		storeInventory.remove("test1");
		assertNull("Item wasn't removed", storeInventory.find("test1"));
	}

	/*
	 * Test 7: Testing the ability to subtract from an Item's quantity
	 */
	@Test
	public void testSubtractQuantity() throws StockException {
		storeInventory.add(test[2], 1);
		storeInventory.subtract("test2", 1);
		assertEquals("Item quanitity is wrong", new Integer(0),
				storeInventory.getItemQuantity(storeInventory.find("test2")));
	}

	/*
	 * Test 8: Testing the inability to give an item negative stock
	 */
	@Test(expected = StockException.class)
	public void testSubtractQuantityFail() throws StockException {
		storeInventory.subtract("test3", 2);
	}

	/*
	 * Test 9: Test ToCSVString
	 */
	@Test
	public void testCSVToString() {
		String t = "test0,0" + System.lineSeparator() + "test1,0" + System.lineSeparator() + "test2,0"
				+ System.lineSeparator() + "test3,0" + System.lineSeparator() + "test4,0" + System.lineSeparator()
				+ "test5,0" + System.lineSeparator() + "test6,0" + System.lineSeparator() + "test7,0"
				+ System.lineSeparator() + "test8,0" + System.lineSeparator() + "test9,0" + System.lineSeparator()
				+ "test10,0" + System.lineSeparator() + "test11,0" + System.lineSeparator() + "test12,0"
				+ System.lineSeparator() + "test13,0" + System.lineSeparator() + "test14,0";
		assertEquals("Strings aren't equal", t, storeInventory.ToCsvString());
	}
}
