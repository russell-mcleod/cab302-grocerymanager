package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import delivery.OrdinaryTruck;
import delivery.Truck;
import exceptions.StockException;
import exceptions.TemperatureException;
import exceptions.TruckException;
import stock.Item;
import stock.Stock;

/**
 * JUnit Test Class to test OrdinaryTruck class
 * 
 * @author Russell McLeod
 *
 */
public class TestOrdinaryTruck {

	Truck testTruck1;

	/**
	 * Test 0: Constructor Test
	 */
	@Before
	@Test
	public void testInitTruck() {
		testTruck1 = new OrdinaryTruck();
	}

	/**
	 * A Rule used by later tests to check Exception Types and their Messages
	 */
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	/**
	 * Test 1: Testing to add temperature-controlled Item to Ordinary Truck
	 * 
	 * @throws TemperatureException
	 *             A TemperatureException due to trying to add
	 *             temperature-controlled Items to an Ordinary Truck
	 * @throws TruckException
	 *             TruckException due to adding too much cargo too a Truck.
	 */
	@Test
	public void testAddCargoWithTemperature() throws TemperatureException, TruckException {
		thrown.expect(TemperatureException.class);
		String message = "Failed to add Cargo: Cargo contains a temperature-controlled Item!" + System.lineSeparator()
				+ "Unsupported Item in Cargo: " + "Milk";
		thrown.expectMessage(message);

		Stock cargo = new Stock();
		cargo.add(new Item("Milk", 2.0, 3.0, 200, 300, 3));
		cargo.add(new Item("Bread", 1.0, 1.5, 100, 200));
		testTruck1.addCargo(cargo);

		fail("Expected TemperatureException to be thrown: temperature-controlled Items cannot be put in Ordinary Truck.");
	}

	/**
	 * Test 2: Testing getter for truck's capacity (should be constant)
	 */
	@Test
	public void testTruckCapacity() {
		assertEquals("Failed to get correct constant capacity for OrdinaryTruck.", 1000, testTruck1.getCapacity());
	}

	/**
	 * Test 3: Testing getting the truck's cargo
	 * 
	 * @throws TemperatureException
	 *             A TemperatureException due to trying to add
	 *             temperature-controlled Items to an Ordinary Truck
	 * @throws TruckException
	 *             TruckException due to adding too much cargo too a Truck.
	 */
	@Test
	public void testAddGetCargo() throws TemperatureException, TruckException {
		Stock cargo = new Stock();

		cargo.add(new Item("Candybar", 2.0, 3.0, 200, 300));
		cargo.add(new Item("Bread", 1.0, 1.5, 100, 200));

		testTruck1.addCargo(cargo);
		assertEquals("Failed to get correct cargo Stock from Truck.", cargo.getAllItems(),
				testTruck1.getCargo().getAllItems());
	}

	/*
	 * Test 4: Testing getting the truck's cost.
	 * 
	 */
	@Test
	public void testGetCost() throws TruckException, TemperatureException, StockException {
		Stock cargo = new Stock();

		Item candybar = new Item("Candybar", 2.0, 3.0, 200, 300);

		cargo.add(candybar);
		cargo.add(new Item("Bread", 1.0, 1.5, 100, 200));
		cargo.add(candybar);
		cargo.add(candybar);
		cargo.add(candybar);
		cargo.add(candybar);

		testTruck1.addCargo(cargo);
		double calculatedCost = 750.0 + (0.25 * testTruck1.getCargo().getQuantity());

		assertEquals("Failed to get correct Truck Cost.", calculatedCost, testTruck1.getCost(), 0);
	}

	/**
	 * Test 6: Testing adding cargo to a truck that is already full.
	 * 
	 * @throws TruckException
	 *             A TruckException due to trying to add cargo to a truck that is
	 *             already full.
	 * @throws TemperatureException
	 *             A TemperatureException due to trying to add
	 *             temperature-controlled Items to an Ordinary Truck
	 */
	@Test
	public void testAddCargoWhenFull() throws TruckException, TemperatureException {
		thrown.expect(TruckException.class);
		String message = "Failed to add Cargo: Truck has not enough space.";
		thrown.expectMessage(message);

		Stock cargo = new Stock();

		for (int i = 0; i < testTruck1.getCapacity() + 10; i++) {
			cargo.add(new Item("NewItem" + i, 1.0, 50.0, 500, 1000), 1);
		}
		testTruck1.addCargo(cargo);
		System.out.println(testTruck1.getCargo().getQuantity());
		fail("Expected TruckException to be thrown: cannot add cargo to a full Truck.");
	}
}
