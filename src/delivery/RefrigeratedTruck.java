package delivery;

import exceptions.TemperatureException;
import exceptions.TruckException;
import stock.Item;
import stock.Stock;

/**
 * A truck with a controlled temperature
 * 
 * @author Cain Kroker
 *
 */
public class RefrigeratedTruck extends Truck {

	/**
	 * The Temperature of the Refrigerated Truck.
	 */
	private Integer temperature;

	/**
	 * The capacity (amount of items that can be added) of the Refrigerated Truck.
	 */
	private final int capacity = 800;

	/**
	 * First constructor: Sets the temperature to the provided value.
	 * 
	 * @param temperature
	 *            The provided temperature
	 * @throws TemperatureException
	 *             Thrown when the temperature is outside the allow range.
	 */
	public RefrigeratedTruck(Integer temperature) throws TemperatureException {
		if (temperature < -20 || temperature > 10) {
			throw new TemperatureException("Temperature is outside the allowed range!" + System.lineSeparator()
					+ "Attempted temperature: " + temperature.toString() + System.lineSeparator()
					+ "Allowed Range: -20 Celsius to 10 Celsius.");
		}
		this.temperature = temperature;
	}

	/**
	 * Final constructor: Sets the temperature to the provided value as well as
	 * providing already existing stock.
	 * 
	 * @param cargo
	 *            Existing stock
	 * @param temperature
	 *            Provided temperature
	 * @throws TruckException
	 *             Inherited from RefrigeratedTruck#addCargo(cargo)
	 * @throws TemperatureException
	 *             Thrown when the temperature is outside the allow range.
	 */
	public RefrigeratedTruck(Stock cargo, Integer temperature) throws TruckException, TemperatureException {
		if (temperature < -20 || temperature > 10) {
			throw new TemperatureException("Temperature is outside the allowed range!" + System.lineSeparator()
					+ "Attempted temperature: " + temperature.toString() + System.lineSeparator()
					+ "Allowed Range: -20 Celsius to 10 Celsius.");
		}
		this.temperature = temperature;
		this.addCargo(cargo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see delivery.Truck#addCargo(stock.Stock)
	 */
	@Override
	public void addCargo(Stock c1) throws TruckException, TemperatureException {
		for (Item item : c1.getAllItems()) {
			Integer itemQuantity = c1.getItemQuantity(item);

			if (item.getTemperature() != null && item.getTemperature() < this.getTemperature()) {
				String exMessage = "Temperature of a Cargo Item needs to be lower than this Truck supports!"
						+ System.lineSeparator() + "Attempted Item: " + item.getName() + System.lineSeparator()
						+ "Item Temperature: " + item.getTemperature() + System.lineSeparator() + "Allowed Range: "
						+ this.getTemperature() + " Celsius and higher.";
				throw new TemperatureException(exMessage);
			}

			if (capacity - currentLoad < itemQuantity) {
				String exMessage = "Failed to add Cargo: Truck has not enough space." + System.lineSeparator()
						+ "Item: " + item.getName() + " Quantity: " + itemQuantity + System.lineSeparator()
						+ "Remaining Room left in Truck: " + (capacity - currentLoad);
				throw new TruckException(exMessage);
			}

			this.cargo.add(item, itemQuantity);
			currentLoad += itemQuantity;
		}
	}

	/**
	 * Gets the current load
	 * 
	 * @return The current load
	 */
	public int getCurrentLoad() {
		return currentLoad;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see delivery.Truck#getCost()
	 */
	@Override
	public double getCost() {
		return 900.0 + (200.0 * Math.pow(0.7, (this.temperature / 5.0)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see delivery.Truck#getCapacity()
	 */
	@Override
	public int getCapacity() {
		return this.capacity;
	}

	/**
	 * Gets the Temperature of the Refrigerated Truck.
	 * 
	 * @return The Temperature of this RefrigeratedTruck.
	 */
	public int getTemperature() {
		return this.temperature;
	}
}
