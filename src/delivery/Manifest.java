package delivery;

import java.util.ArrayList;

/**
 * The manifest class that holds all the trucks
 * 
 * @author Cain Kroker
 *
 */
public class Manifest {
	public static ArrayList<Truck> manifest = new ArrayList<Truck>();

	public Manifest() {
	}

	/**
	 * Adds a truck to the collection
	 * 
	 * @param t
	 *            The truck to add
	 */
	public static void add(Truck t) {
		manifest.add(t);
	}

	/**
	 * Removes a truck from the collection
	 * 
	 * @param t
	 *            The truck to remove
	 */
	public static void remove(Truck t) {
		manifest.remove(t);
	}

	/**
	 * Checks to see if the the collection contains that truck
	 * 
	 * @param t
	 *            The truck to check
	 * @return True if it contains the truck, false otherwise.
	 */
	public static boolean contains(Truck t) {
		return manifest.contains(t);
	}

	/**
	 * Gets a truck at a specific position
	 * 
	 * @param pos
	 *            The position of the truck
	 * @return The truck at that position
	 */
	public static Truck get(int pos) {
		return manifest.get(pos);
	}

	/**
	 * Gets the total price for the Manifest, based on the cost of all the contained
	 * Trucks.
	 * 
	 * @return The total cost of all the Trucks in the Manifest.
	 */
	public static double getTotalPrice() {
		double totalPrice = 0.0;
		for (Truck t : manifest) {
			totalPrice += t.getCost();
		}
		return totalPrice;
	}

	/**
	 * Converts the collection of trucks as well as their cargo into a String to be
	 * used when saving to a CSV.
	 * 
	 * @return The CSV-formatted String representaton of the Manifest.
	 */
	public static String toCsvString() {
		String value = "";
		for (Truck i : manifest) {
			value += ">" + i.getClass().getSimpleName().split("T")[0] + System.lineSeparator();
			value += i.getCargo().ToCsvString() + System.lineSeparator();
		}
		return value;
	}
}
