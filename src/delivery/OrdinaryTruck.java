package delivery;

import exceptions.TemperatureException;
import exceptions.TruckException;
import stock.Item;
import stock.Stock;

/**
 * An ordinary truck that can't store temperature controlled items.
 * 
 * @author Cain Kroker
 *
 */
public class OrdinaryTruck extends Truck {
	private final int capacity = 1000;

	/**
	 * Initial constructor for the OrdinaryTruck where the cargo isn't supplied to
	 * begin with.
	 */
	public OrdinaryTruck() {
		currentLoad = 0;
	}

	/**
	 * Another constructor for the OrdinaryTruck where the cargo is supplied at
	 * creation.
	 * 
	 * @param cargo
	 *            The supplied cargo.
	 * @throws TruckException
	 *             Inherited fromOrdinaryTruck#addCargo(cargo)
	 * @throws TemperatureException
	 *             Inherited from OrdinaryTruck#addCargo(cargo)
	 */
	public OrdinaryTruck(Stock cargo) throws TruckException, TemperatureException {
		this.addCargo(cargo);
		currentLoad = cargo.getQuantity();
	}

	@Override
	public void addCargo(Stock c1) throws TruckException, TemperatureException {
		for (Item item : c1.getAllItems()) {
			Integer itemQuantity = c1.getItemQuantity(item);

			if (item.getTemperature() != null) {
				String exMessage = "Failed to add Cargo: Cargo contains a temperature-controlled Item!"
						+ System.lineSeparator() + "Unsupported Item in Cargo: " + item.getName();
				throw new TemperatureException(exMessage);
			}

			if (capacity - currentLoad < itemQuantity) {
				String exMessage = "Failed to add Cargo: Truck has not enough space." + System.lineSeparator()
						+ "Item: " + item.getName() + " Quantity: " + itemQuantity + System.lineSeparator()
						+ "Remaining Room left in Truck: " + (capacity - currentLoad);
				throw new TruckException(exMessage);
			}

			this.cargo.add(item, itemQuantity);
			currentLoad += itemQuantity;
		}
	}

	@Override
	public double getCost() {
		return 750.0 + (0.25 * this.cargo.getQuantity());
	}

	@Override
	public int getCapacity() {
		return this.capacity;
	}

	/**
	 * Gets the current load
	 * 
	 * @return The current load
	 */
	public int getCurrentLoad() {
		return this.currentLoad;
	}
}
