package delivery;

import exceptions.TemperatureException;
import exceptions.TruckException;
import stock.Item;
import stock.Stock;

/**
 * This is the abstract class that will be used as the base of both the
 * OrdinaryTruck and the RefrigeratedTruck.
 * 
 * @author Cain Kroker
 *
 */
public abstract class Truck {

	/**
	 * Main Stock var to hold all the Stock in the Truck.
	 */
	protected Stock cargo = new Stock();

	/**
	 * The current quantity of items loaded inside the Truck.
	 */
	protected int currentLoad;

	/**
	 * Default constructor for Truck.
	 */
	public Truck() {
		cargo = new Stock();
	}

	/**
	 * A second constructor that can also be used by all versions of a truck. It
	 * will be used when the stock is decided before the truck is created.
	 * 
	 * @param cargo
	 *            The cargo of said truck.
	 */
	public Truck(Stock cargo) {
		this.cargo = cargo;
	}

	/**
	 * Allows for the ability to add more cargo if the truck hasn't reached the
	 * maximum capacity/the truck doesn't have an cargo at all.
	 * 
	 * @param cargo
	 *            The cargo to add.
	 * @throws TruckException
	 *             The exception in the case where cargo cannot be added, likely due
	 *             to limited capacity.
	 * @throws TemperatureException
	 *             The exception in the case where cargo cannot be added, due to
	 *             Temperature-related issues.
	 */
	public abstract void addCargo(Stock cargo) throws TruckException, TemperatureException;

	/**
	 * The total cost of running the truck.
	 * 
	 * @return The cost
	 */
	public abstract double getCost();

	/**
	 * Gets the maximum capacity
	 * 
	 * @return The maximum capacity
	 */
	public abstract int getCapacity();

	/**
	 * Gets the current cargo if available
	 * 
	 * @return The current cargo
	 */
	public Stock getCargo() {
		return this.cargo;
	}

	/**
	 * Splits a Stock that would normally exceed the Truck's capacity.
	 * 
	 * @param c1
	 *            The Stock to split.
	 * @return The split part of the given Stock, that can fit inside of the Truck.
	 */
	public Stock splitStock(Stock c1) {
		Stock splitStock = new Stock();
		int tempLoad = 0;
		for (Item item : c1.getAllItems()) {
			if (c1.getItemQuantity(item) + tempLoad < this.getCapacity()) {
				splitStock.add(item, c1.getItemQuantity(item));
				tempLoad += c1.getItemQuantity(item);
			} else if (tempLoad < this.getCapacity() && c1.getItemQuantity(item) + tempLoad > this.getCapacity()) {
				splitStock.add(item, (this.getCapacity() - tempLoad));
				tempLoad += (this.getCapacity() - tempLoad);
			}
		}
		return splitStock;
	}

	/**
	 * Splits a Stock that would normally exceed the Truck's capacity.
	 * 
	 * @param c1
	 *            The Stock to split.
	 * @param currentLoad
	 *            An integer that represents what the trucks current load is
	 * @return The split part of the given Stock, that can fit inside of the Truck.
	 */
	public Stock splitStock(Stock c1, int currentLoad) {
		Stock splitStock = new Stock();
		int tempLoad = currentLoad;
		for (Item item : c1.getAllItems()) {
			if (c1.getItemQuantity(item) + tempLoad < this.getCapacity()) {
				splitStock.add(item, c1.getItemQuantity(item));
				tempLoad += c1.getItemQuantity(item);
			} else if (tempLoad < this.getCapacity() && c1.getItemQuantity(item) + tempLoad > this.getCapacity()) {
				splitStock.add(item, (this.getCapacity() - tempLoad));
				tempLoad += (this.getCapacity() - tempLoad);
			}
		}
		return splitStock;
	}
}
